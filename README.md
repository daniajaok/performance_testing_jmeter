# Pengujian API RESTful menggunakan JMeter pada website https://dummyjson.com/
Ini adalah proyek pengujian API sederhana menggunakan JMeter untuk menguji endpoint web https://dummyjson.com/. DummyJSON adalah web yang menyediakan api gratis yang dapat digunkan secara publik. Pengujian API ini bertujuan untuk memvalidasi respons dari berbagai endpoint yang disediakan oleh DummyJSON.

## Cara Penggunaan:

    1. Pastikan Anda memiliki JMeter diinstal di sistem Anda. Jika belum, Anda dapat mengunduhnya dari situs web resminya: JMeter Apache.
    2. Unduh file User Collections.jmx yang disediakan dalam repositori ini.
    3. Buka JMeter dan impor file User Collections.jmx yang telah Anda unduh.
    4. Sesuaikan pengaturan tes sesuai kebutuhan Anda, seperti jumlah pengguna bersamaan waktu pengujian, dan endpoint yang ingin Anda uji.
    5. Jalankan pengujian.
    6. Periksa laporan hasil untuk melihat apakah endpoint DummyJSON merespons dengan benar sesuai harapan Anda.
Catatan:

Pastikan untuk memperbarui endpoint dan konfigurasi pengujian sesuai dengan kebutuhan dan spesifikasi proyek Anda sebelum menjalankan pengujian. Jika Anda memiliki pertanyaan lebih lanjut atau masalah saat menggunakan skrip pengujian, jangan ragu untuk menghubungi saya.

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/muhammad-ramdhani-006aa8178/)